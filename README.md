# Imports
## Librairies


```python
# NUMPY
import numpy as np

# STATS
import scipy.stats as stats
from scipy.stats import norm, skew

# MATPLOTLIB
import matplotlib as mlp
import matplotlib.pyplot as plt
plt.style.use('fivethirtyeight') 

# WARNINGS
import warnings
warnings.filterwarnings('ignore')

# PANDAS
import pandas as pd 
pd.set_option("display.max_rows", None, "display.max_columns", None) 

# SEABORN
import seaborn as sns


# SCIKIT-LEARN: SELECTION DE VARIABLES

from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2

# SCIKIT-LEARN: PRE-PROCESSING
from sklearn.preprocessing import LabelEncoder, OrdinalEncoder # Encodage des variables catégorielles ordinales
from sklearn.preprocessing import LabelBinarizer, OneHotEncoder # Encodage des variables catégorielles nominales
from sklearn.preprocessing import StandardScaler # Normalisation des variables numériques
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import RobustScaler
from sklearn.impute import SimpleImputer # Imputation
from sklearn.impute import KNNImputer 


# SCIKIT-LEARN: MODELES
from sklearn import linear_model # Classe Modèle linéaire 
from sklearn.linear_model import LinearRegression, Ridge, Lasso, ElasticNet # Régression linéaire
from sklearn.linear_model import LogisticRegression # Régression logistique
from sklearn.svm import LinearSVC, SVC # Machines à vecteurs de support

# SCIKIT-LEARN: VALIDATION CROISEE + OPTIMISATION
from sklearn.model_selection import train_test_split # Découpage en données train et test
from sklearn.model_selection import cross_val_score # Validation croisée pour comparaison entre modèles
from sklearn.model_selection import validation_curve # Courbe de validation: visulaisr les scores lors du choix d'un hyperparamétre
from sklearn.model_selection import GridSearchCV # Tester plusieurs hyper_paramètres
from sklearn.model_selection import learning_curve # Courbe d'apprentissage: visualisation les scores du train et du validation sets en fonction des quanitiés des données
 
## EVALUATION
from sklearn.metrics import accuracy_score # Exactitude (accuracy)
from sklearn.metrics import f1_score # F1-score
from sklearn.metrics import confusion_matrix # Matrice de confusion
from sklearn.metrics import plot_confusion_matrix # Graphique de la matrice de confusion
from sklearn.metrics import classification_report # Rapport pour le modèle de classification

## EVALUATION: COURBE ROC
from sklearn.metrics import auc # Aire sous la courbe 
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve 

# SCHIKIT-LEARN: PIPELINE et TRANSFORMATEUR
from sklearn.pipeline import make_pipeline
from sklearn.compose import make_column_transformer

# WARNINGS
import warnings
warnings.filterwarnings('ignore')
```

## Constantes


```python
# Path vers le dataset
_DATASET_PATH_ = None
# Setting du random state pour la reproduction à l'identique des tests
_RANDOM_STATE_ = 7
# Nom de la target
targetName = 'target'
```

## Datasets


```python
# Fonctions d'import
# si XLS :
dataset = pd.read_excel(_DATASET_PATH_)
# si CSV :
dataset = pd.read_csv(_DATASET_PATH_)
```


```python
# Copie du dataset
df_data = dataset.copy()
```

# Analyse exploratoire des données
## Données générales sur le dataset


```python
# Afficher la taille du dataset
n_samples, n_features = df_data.shape
```


```python
# Afficher le type des variables, ajouter .value_counts() pour voir le nombre de chaque type
df_data.dtypes
```


```python
# Séparer les labels de variables catégoriques et continues
cat_features = df_data.select_dtypes('float64', 'int64').columns.drop(labels=targetName,errors='ignore')
con_features = df_data.select_dtypes('object').columns.drop(labels=targetName,errors='ignore')

# Si besoin de retirer une colonne pour la passer dans l'autre catégorie :
# cat_features = cat_features.append(pd.Index(['nomColonne']))
# con_features = con_features.delete(pd.Index(['education']))
```


```python
# Obtenir des informations sur le DataFrame en général
df_data.info()
```

## Analyse univariée
### Target (classification)


```python
# Permet de voir la répartition d'une classe et detecter si elle est équilibrée ou non, on peut ajouter
# .value_counts(normalize=True) pour avoir la stat normalisée entre 0 et 1
df_data[targetName].value_counts()
```

Cette analyse sur la target dans un cas de classification permet de détecter si la classe est équilibrée. Si ce n'est pas le cas, il faudra le prendre en compte pour la partie de préprocess afin de rééquilibrer les classes, ou alors choisir les bonnées métriques (exactitude si équilibrée, sinon précision, rappel etc).


```python
# Graphique en barres et en camembert représentant la répartition des valeurs de la target
plt.figure(figsize=(8, 4))
plt.subplots_adjust(right=1.5)
plt.subplot(121)
sns.countplot(x=targetName, data=df_data)
plt.title("Distribution de la variable cible:")
plt.subplot(122)
plt.pie(df_data[targetName].value_counts(), autopct='%1.1f%%')
plt.show()
```

### Target (régression)


```python
# Afficher la description statistique de la target
df_data[targetName].describe()
```


```python
# Afficher la répartition dans un boxplot pour voir comment est étalée la variable
plt.figure()
sns.boxplot(df_data[targetName])
plt.show()
```

Permet de voir si il y'a des outliers, et l'IQ.


```python
# Afficher la répartition des valeurs de la target pour vérifier la normalité
sns.distplot(df_data[targetName], fit=norm);
```

Ce graph superposant la gaussienne à la répartition de la target permet de vérifier la normalité de la target, qui est une hypothèse nécessaire pour réaliser une régression cohérente.


```python
# Affichage du Q-Q plot
plt.figure()
stats.probplot(df_data[targetName], plot=plt)
plt.show()
```

Permet de voir la probabilité d'obtenir une valeur pour la target selon les quantiles calculés. Dans une répartition normale, on doit obtenir une droite.

### Variables continues
#### Description statistique


```python
# Afficher la description statistique des variables
df_data[con_features].describe()
```

#### Histogrammes


```python
# Afficher l'histogramme de chaque variables continues
for col in con_features:
    plt.figure()
    sns.distplot(df_data[col])
    # plt.hist(df_data[col], bins=20, alpha=1, density=True)
    plt.tight_layout()
    plt.show()
```

Ces histogrammes permet de voir si les répartitions sont normales. Si c'est le cas, on peut supposer qu'elles ont été standardisées.

### Variables catégorielles
#### Liste des valeurs différentes


```python
# Permet d'afficher les valeurs prises par chaques variables pour voir l'étendue des dictionnaires
for col in cal_features:
    print(f'{col :-<70} {df_data[col].unique()}') # créer un sytème de marge
```

#### Camemberts


```python
# Permet d'afficher un diagramme en camembert de toutes les variables catégorielles (répartition des valeurs)
for col in cal_features:
    plt.figure()
    df_data[col].value_counts().plot.pie()
    plt.show()
```

L'analyse faite à partir de ces données permet d'émettre une hypothèse sur les variables intéressantes, par exemples si elles sont binaires, ou possèdent beaucoup d'observations d'une valeur et peu d'une autre par exemple.
## Analyse bivariée générale
### Variables continues-continues
#### Matrice de corrélation


```python
# Permet d'obtenir la matrice de corrélation des variables continues
df_data[con_features].corr()
```

La matrice de corrélation permet d'observer la corrélation linéaire existante ou non entre les variables numériques deux à deux. La force de la corrélation varie entre -1 et 1, avec 0 s'il n'y a aucune colinéarité.

#### Pairplot


```python
# Afficher le pairplot de relation entre toutes les variables numériques
plt.figure()
sns.pairplot(df_data[con_features])
plt.show()
```

L'affichage de pairplot permet de détecter des relations de colinéarité entre certaines variables.
#### Heatmap


```python
# Permet d'afficher sous forme de heatmap la corrélation linéaire entre les variables
# On peut remplacer par sns.clustermap(df_data[con_features].corr()) pour avoir les clusters
plt.figure(figsize=(26, 12))
sns.heatmap(df_data[con_features].corr(), annot=True, cbar=False)
plt.show()
```

Les graphiques Heatmap permettent d'observer la corrélation linéaire existante ou non entre les variables numériques deux à deux. La force de la corrélation varie entre -1 et 1, avec 0 s'il n'y a aucune colinéarité.

#### Lmplot


```python
# Distribution de label1 et label2 (quantitatives) par rapport à label3 et label4 (binaires)
plt.figure()
sns.lmplot('label1', 'label2', data=df_data, hue=targetName, col='label3', row='label4')
plt.show()
```

### Variables categorielles-categorielles
#### Tableau croisé


```python
# Permet d'afficher le tableau croisé entre une variable catégorielle label 1 et une autre label2
pd.crosstab(dfcov['label1'], dfcov['label2'])
```

Cette analyse par tableau croisé permet d'évaluer à l'oeil la corrélation entre des variables catégorielles.
#### Heatmap


```python
# Afficher une heatmap avec chaque variable catégorielle et la target
for feature in cat_features:
    for feature2 in cat_features:
        plt.figure()
        sns.heatmap(pd.crosstab(df_data[feature2], df_data[feature]), annot=True, fmt='d')
        plt.show()
```

La heatmap dans ce cas permet de voir de manière plus graphique le tableau croisé précédent.
### Variables categorielles-continues
#### Graph de densité selon les classes


```python
# On commence par séparer sur une feature donnée en toutes les values possibles
df_data_label1 = df_data[df_data['featureEtudiee'] == 'label1']
df_data_label2 = df_data[df_data['featureEtudiee'] == 'label2']
# On affiche ensuite les graphs superposés
for feature in con_features:
    plt.figure()
    sns.distplot(df_data_label1[feature], label='label1')
    sns.distplot(df_data_label2[feature], label='label2')
    plt.legend()
    plt.show()
```

La superposition des valeurs prises par les classes catégorielles selon les valeurs prises par les classes continues permet de voir si un facteur est corrélé plus ou moins à un autre.

## Analyse bivariée (classification)
### Corrélation entre variables catégoriques et la target


```python
# Afficher une heatmap avec chaque variable catégorielle et la target
for feature in cat_features:
    plt.figure()
    sns.heatmap(pd.crosstab(df_data[targetName], df_data[feature]), annot=True, fmt='d')
    plt.show()
```

La heatmap permet de voir à quel point la corrélation linéaire entre la target et les features est importante.


```python
# Afficher le diagramme en batons de correlation entre une variable continue et la target
# /!\ Ne fonctionne que sur les variables entières avec peu de valeurs différentes, ou les variables catégorielles
plt.figure()
sns.countplot(x='feature', hue=targetName, data=df_data)
plt.show()
```

L'affichage de ce diagramme permet de voir si la répartition des valeurs de la target varie selon les valeurs prises par une variable donnée.

### Corrélation entre variables continues et la target


```python
# Permet d'aficher les valeurs de la variable cible selon la répartition de chaque feature continues
# On crée des Series avec chaque valeur de la target
df_data_label1 = df_data[df_data[targetName] == 'label1']
df_data_label2 = df_data[df_data[targetName] == 'label2']
# Remplacer df_data_label1 et df_data_label2 par les vrais labels
for feature in con_features:
    plt.figure()
    sns.distplot(df_data_label1[feature], label='label1')
    sns.distplot(df_data_label2[feature], label='label2')
    plt.legend()
    plt.show()
```

Cette analyse permet de déterminer si la répartition des valeurs de la target changent beaucoup quand les valeurs d'une feature varie, pour en déduire un éventuel lien de regression possible.


```python
# Afficher le diagramme en batons de correlation entre une variable continue et la target
# /!\ Ne fonctionne que sur les variables entières avec peu de valeurs différentes, ou les variables catégorielles
plt.figure()
sns.countplot(x='feature', hue=targetName, data=df_data)
plt.show()
```

L'affichage de ce diagramme permet de voir si la répartition des valeurs de la target varie selon les valeurs prises par une variable donnée.

## Analyse bivariée (regression)
### Corrélation entre variables continues et la target


```python
# Afficher le scatter entre une variable quantitative label1 et la target
df_data.plot.scatter(x='label1', y=targetName);
```

Ce graph permet de detecter une éventuelle corrélation entre les valeurs d'une feature et celles de la target.
### Corrélation entre variables continues et la target


```python
# Afficher le boxplot de répartition des valeurs de la target selon les classes d'une feature catégorique label1
plt.figure(figsize=(16, 8))
sns.boxplot(x='label1', y=targetName, data=df_data)
plt.show()
```

Ce graph permet visuellement d'émettre une hypothèse sur la corrélation entre les valeurs prises par la target et une feature catégorique.


```python
# Si les variables sont des catégories sous forme d'entiers, on peut utiliser ce graph de corrélation
# avec la target
plt.figure(figsize=(16, 16))
df_data.corr()[targetName].sort_values(ascending=True)[:-1].plot(kind='barh')
plt.title("Corrélation entre les variables explicatives et la cible")
plt.tight_layout()
plt.show()
```

Comme le graph précédent, celui-ci permet d'évaluer la force de colinéarité entre les features et la target.

## Tests statistiques
### Test d'indépendance entre une feature categorielle et la target (classification)

Pour réaliser un test statistique, on pose une hypothèse H0, qu'on confrontera à une hypothèse H1. Cela permettra de savoir si on doit rejeter H0 ou non.

**(H0)** : Les valeurs prises par la feature sont indépendants des valeurs prises par les classes de la target.

**(H1)** : Les valeurs prises par la feature sont dépendants des valeurs prises par les classes de la target


```python
# On importe tout d'abord le ttest_ind de scipy
from scipy.stats import ttest_ind
# On sépare la target selon les valeurs prises par cette dernière
df_data_label1 = df_data[df_data[targetName] == 'label1']
df_data_label2 = df_data[df_data[targetName] == 'label2']
# On prend des échantillons de meme taille pour toutes valeurs de la target, pour rééquilibrer,
# basé sur la classe sous représentée, pour le sous échantilloner
n_samples_label1, _ = df_data_label1.shape
df_data_label2_sample = df_data_label2.sample(n_samples_label1)
# On réalise un test de Student, en fixant un alpha de tolérance qui devra être strictement supérieure
# à la pvalue obtenue par le test afin de rejeter le test
def t_test(col):
    alpha = 0.02# 2%
    stat, p = ttest_ind(df_data_label2[col].dropna(), df_data_label1[col].dropna())
    if p < alpha:
        return 'H0 Rejetée'
    else:
        return 0
# On applique la fonction de test
for feature in con_features:
    print(f'{feature:-<70} {t_test(feature)}')
```

**/!\** Ne pas rejeter H0 ne signifie pas qu'il y'a indépendance, simplement que cette dernière n'a pas été prouvée par le test. Un autre test donnera peut-être d'autres résultats.

# Preprocess

### Elimination des colonnes jugées inutiles


```python
nomColonneASupprimer = None
df_data = df_data.drop(nomColonneASupprimer, axis=1)
```

**/!\** Il faut supprimer des indexs les colonnes supprimées.

### Valeurs manquantes


```python
# Afficher tous les na
plt.figure(figsize=(26, 12))
sns.heatmap(df_data.isna(), cbar=False)
plt.show()
```


```python
# Afficher les na par ordre décroissant de pourcentage
dfMissvalues = pd.DataFrame(
                           (round(100* df_data.isnull().sum()/len(df_data), 2)), 
                           columns=['pourcentage vals manqutes']
                           )
dfMissvalues.sort_values(by=['pourcentage vals manqutes'], ascending=False).head(20)
```


```python
# Suppression des colonnes dont le taux de valeurs manquantes est supérieur à un seuil
thresholdNa = 0.9
df_data = df_data.columns[df_data.isna().sum()/n_samples < thresholdNa]
```

## Split du training set et du testing set


```python
df_train, df_test = train_test_split(df_data, test_size=0.2, random_state=_RANDOM_STATE_, stratify=df_data[targetName])
```

## Préprocess généraux

### Selection automatique des meilleurs colonnes (classification)


```python
# On coupe le dataset pour utiliser le selecteur
df_train_select = df_train.drop(targetName, axis=1)
target_train = df_train[targetName]
# Appliquer une sélection des variables et extraire n variables
n = 10
select = SelectKBest(score_func=chi2, k=n)
z = select.fit_transform(df_train_select, target_train)
# On extrait les scores de selection pour évaluer l'intérêt
selection_scores = pd.DataFrame(select.scores_)
data_columns = pd.DataFrame(df_train_select.columns)
# Concaténer les deux dataframes
scores = pd.concat([data_columns,selection_scores],axis=1)
scores.columns=['Feature','Score']
print(scores.nlargest(11,'Score'))
# Visualiser la sélection de variables
scores = scores.sort_values(by="Score", ascending=False)
plt.figure(figsize=(20,7), facecolor='w')
sns.barplot(x='Feature',y='Score',data=scores,palette='BuGn_r')
plt.title("Plot showing the best features in descending order", size=20)
plt.show()
```

### Elimination d'outliers choisis arbitrairement


```python
# Éliminer quelques outliers de label1 et label2 selon des seuils donnés
df_train = df_train.drop(df_train[(df_train['label1'] > 4000) 
                                & (df_train['label2'] <300000)].index)
```

### Elimination des outliers au dela de 1.5 fois l'IQ du 1er et 3e quartiles


```python
# Suppression des outliers calculés via l'IQ
for feature in con_features:
    Q1 = df_train[feature].quantile(0.25)
    Q3 = df_train[feature].quantile(0.75)
    IQR = Q3 - Q1
    df_train = df_train[(df_train[feature] >= Q1 - 1.5*IQR) & (df_train[feature] <= Q3 + 1.5*IQR)]
```

### Log transformation de la target (régression, pour normaliser la répartition)


```python
# On verifie que le coefficient d'asymétrie et le coefficient d'applatissment 
print("Skewness: %f" %df_train[targetName].skew())
print("kurtosis: %f" %df_train[targetName].kurt())
# On importe kstest de stats
from scipy.stats import kstest
loc, scale = norm.fit(df_train[targetName])
n = norm(loc=loc, scale=scale)
print(kstest(df_train[targetName], n.cdf))
# Log transformation
df_train[targetName] = np.log1p(df_train[targetName])
```

Il est possible de vérifier le résultat de la transformation en refaisant un test kstest pour vérifier la p_value. Si elle est inférieure à un alpha donné, alors on peut rejeter l'hypothsèe **H0** : "la variable target ne suit pas une loi normale.". On peut aussi retracer un Q-Q plot.

## Imputation

L'imputation permet de remplacer les valeurs manquantes par le mode, la moyenne, la classe la plus fréquente etc.


```python
# Imputation des variables via KNNImputer, ne marche que sur les entiers (k-Nearest Neighbors)
imput_cont = KNNImputer(missing_values=np.nan, n_neighbors=5)
# Application de l'imputer sur les variables continues
for feature in con_features:
    df_train[feature] = imput_cont.fit_transform(df_train[feature].values.reshape(-1,1)).ravel()
    df_test[feature] = imput_cont.transform(df_test[feature].values.reshape(-1,1)).ravel()
```


```python
# Imputation des variables via SimpleImputer
imput_cont = SimpleImputer(missing_values=np.nan, strategy='mean')
imput_cat = SimpleImputer(missing_values=np.nan, strategy='most_frequent')
# Application de l'imputer sur les variables continues
for feature in con_features :
    df_train[feature] = imput_cont.fit_transform(df_train[feature].values.reshape(-1,1)).ravel()
    df_test[feature] = imput_cont.transform(df_test[feature].values.reshape(-1,1)).ravel()
# Application de l'imputer sur les variables catégories 
for feature in cat_features :
    df_train[feature] = imput_cat.fit_transform(df_train[feature].values.reshape(-1,1)).ravel()
    df_test[feature] = imput_cat.transform(df_test[feature].values.reshape(-1,1)).ravel()
```

## Encodage

L'encodage permet de remplacer les strings dans le dataframe par des valeurs numériques (binaires ou des entiers par exemple).


```python
# Label Encoder, permet de remplacer les classes par des entiers dans la colonne donnée
encoder = LabelEncoder()
# Si on a une colonne catégorielle avec deux valeurs par exemple, on peut utiliser l'encoder
df_train['label1'] = encoder.fit_transform(df_train['label1'] )
df_test['label1'] = encoder.transform(df_test['label1'] )
```


```python
# One Hot Encoder, permet de créer autant de colonnes binaires que de valeurs prises par la classe
onehotencoder = OneHotEncoder(sparse=False)
# Si on a une colonne catégorielle avec plusieurs valeurs différentes, on peut utiliser le One Hot Encoder
df_train['label2'] = onehotencoder.fit_transform(df_train['label2'] )
df_test['label2'] = onehotencoder.transform(df_test['label2'] )
```

## Normalisation des valeurs continues

La normalisation permet de recentrer l'interval dans lequel varie une variable continue afin que toutes les valeurs aient le même espace et donc le même poids.


```python
# Normalisation via StandardScaler
scale_std = StandardScaler()
# normaliser les données d'entrainement
for feature in con_features:
    df_train[feature] = scale_std.fit_transform(df_train[feature].values.reshape(-1,1)).ravel()
    df_test[feature] = scale_std.transform(df_test[feature].values.reshape(-1,1)).ravel()
```


```python
# Normalisation via MinMaxScaler
scale_mmx = MinMaxScaler()
# normaliser les données d'entrainement
for feature in con_features:
    df_train[feature] = scale_mmx.fit_transform(df_train[feature].values.reshape(-1,1)).ravel()
    df_test[feature] = scale_mmx.transform(df_test[feature].values.reshape(-1,1)).ravel()
```


```python
# Normalisation via RobustScaler
scale_rbs = RobustScaler()
# normaliser les données d'entrainement
for feature in con_features:
    df_train[feature] = scale_rbs.fit_transform(df_train[feature].values.reshape(-1,1)).ravel()
    df_test[feature] = scale_rbs.transform(df_test[feature].values.reshape(-1,1)).ravel()
```

## Suréchantillonage de la classe minoritaire


```python
# On importe SMOTE de imblearn.over_sampling
from imblearn.over_sampling import SMOTE
smt = SMOTE(random_state=_RANDOM_STATE_)
# Suréchantillonage
X_train_up, y_train_up = smt.fit_resample(X_train, y_train)
```

# Modélisation


```python
# On sépare la target des features
y_train =  df_train[targetName]
y_test =  df_test[targetName]
df_train = df_train.drop(targetName, axis=1)
df_test = df_test.drop(targetName, axis=1)
X_train = df_train
X_test = df_test
```

## Classification

### Modèles de référence (DummyClassifier)


```python
# On importe DummyClassifier de sklearn
from sklearn.dummy import DummyClassifier
# Ce classifier a une équiprobabilité de sortir chaque classe de la target
clf_dummy = DummyClassifier(random_state=_RANDOM_STATE_)
clf_dummy.fit(X_train, y_train)
clf_dummy.score(X_test, y_test)
```

Comme ce classifieur est très simple, on l'utilise comme référence pour les autres classifieurs, qui devront faire mieux que celui-ci.

### Régréssion logisitque


```python
# Modèle sans pénalité
clf_logreg = LogisticRegression(penalty='none', random_state=_RANDOM_STATE_)
```

### Régréssion logistique pénalisée


```python
# Il existe trois modèles principaux : Ridge, Lasso et ElasticNet
clf_logregRidge = LogisticRegression(penalty='l2', random_state=_RANDOM_STATE_) # (penalty='l2') par défaut
clf_logregLasso = LogisticRegression(penalty='l1', solver='saga', random_state=_RANDOM_STATE_)
clf_logregElasticNet = LogisticRegression(penalty='elasticnet', solver='saga', l1_ratio=0.5, random_state=_RANDOM_STATE_)
# Entrainement des modèles
clf_logregRidge.fit(X_train, y_train)
clf_logregLasso.fit(X_train, y_train)
clf_logregElasticNet.fit(X_train, y_train)
```

### Machines à vecteurs de support


```python
# Création de la SVM
penality=8e-2
clf_svm = LinearSVC(C=penality, random_state=_RANDOM_STATE_)
# Entrainement des modèles
clf_svm.fit(X_train, y_train)
```

## Régréssion linéaire

### Régression linéaire simple


```python
# Setup de la régression simple
linreg = LinearRegression()
# Entrainement du modèle
linreg.fit(X_train, y_train)
```

# Evaluation

## Générales

### Courbe d'apprentissage


```python
# On détermine les points de la courbe d'apprentissage, en passant un modèle à entrainer en paramètre
N, train_score, val_score = learning_curve(clf_svm, X_train, y_train,
                                            cv=5,
                                            train_sizes=np.linspace(0.1, 1, 10))

plt.figure(figsize=(12,8))
plt.plot(N, train_score.mean(axis=1), label='train score')
plt.plot(N, val_score.mean(axis=1), label='validation score')
plt.legend()
plt.show()
```

Si le score de la validation croisée ne cesse d'augmenter, c'est qu'il faut sans doute rassembler un plus grand volume de données. Cela permet de vérifier que l'on dispose d'assez de données.

## Classification

### Matrice de confusion, rapport de classification et exactitude


```python
# Prédiction, choisir le modèle
y_pred_train = clf_svm.predict(X_train)
y_pred_test = clf_svm.predict(X_test)
# Affichage de la matrice de confusion
print('='*20)
print('SVM linéaire')
print('='*20, '\n')
print("Matrice de confusion:")
print(confusion_matrix(y_test, y_pred_test), '\n') # afficher à l'écran notre matrice de confusion
print("Rapport de classification:")
print(classification_report(y_test, y_pred_test), '\n')
print('Exactitude: %f' %(accuracy_score(y_test,y_pred_test)*100), '\n')
plot_confusion_matrix(clf_svm, X_test, y_test)
plt.show()
```

### Courbe ROC


```python
# Aire sous la courbe, qu'on peut avoir avec roc_auc_score(y_test, y_pred_test)
# Obtention des points
fpr_train, tpr_train, thresholds_train = roc_curve(y_train, y_pred_train)
fpr_test, tpr_test, thresholds_test = roc_curve(y_test, y_pred_test)
# Affichage de la coubre, avec celle du DummyClassifier
plt.figure(figsize=(14, 7))
plt.plot(fpr_train, tpr_train, label=" AUC du train ="+str(auc(fpr_train, tpr_train)))
plt.plot(fpr_test, tpr_test, label=" AUC du test="+str(auc(fpr_test, tpr_test)))
plt.plot([0,1],[0,1],'g--')
plt.legend()
plt.ylabel("Taux de vrais positifs")
plt.xlabel("Taux de faux positifs")
plt.title("AUC (courbe ROC)")
plt.grid(color='black', linestyle='-', linewidth=0.5)
plt.show()
```

L'AUC est égale à la probabilité que le score d’un exemple classé "label1" à raison soit inférieur à un exemple classé "label2" à tort.

## Régréssion

### Score R2


```python
# Score sur le test d'entraintement du modèle linreg
score = linreg.score(X_train, y_train)
print(score)
```

Ce score évalue la performance du modèle par rapport au niveau de variation présent dans les données.

### Cross validation


```python
# Score sur le test d'entraintement cross-validé du  modèle linreg
score_val = cross_val_score(linreg, X_train, y_train, cv=10, scoring='neg_median_absolute_error')
print(score_val, score__val.mean())
```

Ce score permet de détecter du surapprentissage lors de l'évaluation sur le testing set.

### RMSE


```python
# RMSE
rmse = np.sqrt(mean_squared_error(y_train, y_pred_train))
print(rmse)
```

Ce score permet de donner une idée de la variabilité de la qualité de la prédiction.

### MAE


```python
# Mean Absolute Error
mae = mean_absolute_error(y_train, y_pred_train)
print(mae)
```

Cet indice donne une meilleure idée de la qualité de prédiction, mais n'informe pas sur le surapprentissage ou le sous apprentissage.

### Median AE


```python
# Median Absolute Error 
medae= median_absolute_error(y_train, y_pred_train)
print(medae)
```

### Histogramme de distribution des erreurs


```python
# Distribution des erreurs
hist_erreurs = np.abs(y_train - y_pred_train)
plt.hist(hist_erreurs, bins=50)
plt.show()
```

### Visualisation de la droite de prédiction


```python
# Visualisation de la droite linéaire
plt.rcParams["figure.figsize"] = (12,8)
sns.regplot(x=y_train,y=y_pred_train)
plt.show()
```

Cette visualisation permet de détecter la différence entre la prédiction et la valeur attendue.

### Visualisation de la distribution des résidus


```python
# Distributions des résidus
xmin=10.5
xmax=13.5
plt.rcParams["figure.figsize"] = (12,8)
x_plot = plt.scatter(y_pred_train, (y_pred_train - y_train), c='b')
plt.hlines(y=0, xmin=xmin, xmax=xmax)
plt.title('Résidus')
plt.show()
```

Ce plot permet d'estimer quel interval donne le plus de résidus.

### Poids de régression


```python
# Graphique des coefficients
plt.figure(figsize=(20,8))
predictors = X_train.columns
coef = pd.Series(linreg.coef_,predictors).sort_values()
coef.plot(kind='bar', title='Poids de régression')
plt.show()
```

Ce graph permet de voir l'impact des features sur la regression.

# Optimisation des hyperparamètres

## Classifier

### GridSearchCV


```python
# Setup des paramètres à tester
parameters_svm = {'C': [0.001, 0.01, 0.1, 1, 10]}
# Création de l'optimizer, avec le classifier à utiliser en paramètre
grid = GridSearchCV(estimator=clf_svm, param_grid=parameters_svm, cv=5, scoring='roc_auc')
# Fitting sur le train pour rechercher le meilleur paramètre
grid.fit(X_train, y_train)
# Affichage du meilleur score obtenu
print(grid.best_score_)
# Affichage de l'objet avec le meilleur hyperparamètre testé
grid_best = grid.best_estimator_
print(grid_best)
# Prédiction sur le training set et le testing set pour évaluation
y_pred_train = grid_best.predict(X_train)
y_pred_test = grid_best.predict(X_test)
```

Cette technique permet de tester plusieurs valeurs d'un hyperparamètre afin de l'optimiser, via un quadrillage des hyperparamètres.

### Courbe de validation


```python
# Liste des hyperparamètres à faire varier
list_hyperparams = np.linspace(0.0001, 0.01, 30)
# Training en faisant varier le paramètre
train_score, val_score = validation_curve(clf_svm, 
                                          X_train,
                                          y_train,
                                          param_name='C', 
                                          param_range=list_hyperparams, 
                                          cv=5,
                                         scoring="accuracy",)
# Tracé de la courbe
plt.figure(figsize=(12, 4))
plt.plot(list_hyperparams, train_score.mean(axis = 1), label = 'train')
plt.plot(list_hyperparams, val_score.mean(axis = 1), label = 'validation')
plt.legend()
plt.title("Courbe de validation pour SVM")
plt.ylabel('score')
plt.xlabel('Paramètre de régularisation: ' r'$C$')
plt.show()
```

Ce tracé permet de voir l'impact de la variation d'un hyperparamètre sur les résultats du training.
